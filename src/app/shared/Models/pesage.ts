export class Pesage {
    id: number;
    idPersonne: number;
    poid: number;
    datePesage: Date;
    idUser:number;
    idPersonneNavigation: {
        id: number;
        matricule: string;
        nom: string;
        prenom: string;
        phone: string;
        cin: string;
        cnss: string;
        dateNaissance: string;
        situation: string;
        ville: string;
        adresse: string;
        nombreEnfant: number;
        dateEmbauche: string;
        dateArret: string;
        idbadge:number;
    };
    idUserNavigation:{
        id: number;
        nom: string;
        prenom: string;
        userName: string;
        password: string;
        phone: string;
        role: string;
    }
}
