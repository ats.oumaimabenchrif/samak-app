export class Personne {
    id: number;
    matricule: string;
    nom: string; 
    prenom: string;
    phone: string;
    cin: string;
    cnss: string;
    dateNaissance: string;
    situation: string;
    ville: string;
    adresse: string;
    nombreEnfant: number;
    dateEmbauche: string;
    dateArret: string;
    idbadge:number;
    IdbadgeNavigation:{
        id:number;
        numeroBadge:string;
        numeroSerie:string;
    }
}
