export class User {
    id: number;
    nom: string;
    prenom: string;
    username: string;
    password: string;
    phone: string;
    role: string;
}
