export class Login {
    id: number;
    idUser: string;
    logOut: string;
    dateLogin: string;
    dateLogout: string;
    idUserNavigation: {
        id: number;
        nom: string;
        prenom: string;
        userName: string;
        password: string;
        phone: string;
        role: string;
    }
}
