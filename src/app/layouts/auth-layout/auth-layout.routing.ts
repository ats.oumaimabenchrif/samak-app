import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from 'app/components/auth/login/login.component';

export const AuthLayoutRoutes: Routes = [
  { path: 'login', component: LoginComponent },
];
