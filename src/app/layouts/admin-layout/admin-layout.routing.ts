import { Routes } from '@angular/router';
import { ListPesageComponent } from 'app/components/pesage/liste-pesage/list-pesage.component';
import { AddPersonComponent } from 'app/components/personne/add-person/add-person.component';
import { PersonComponent } from 'app/components/personne/person.component';
import { AddUserComponent } from 'app/components/Users/add-user/add-user.component';
import { ListUserComponent } from 'app/components/Users/list-user/list-user.component';
import { PrixPesageComponent } from 'app/components/pesage/prix-pesage/prix-pesage.component';
import { IconsComponent } from 'app/icons/icons.component';
import { DashboardComponent } from 'app/components/dashboard/dashboard.component';

export const AdminLayoutRoutes: Routes = [

    { path: 'dashboard', component: DashboardComponent },
    { path: 'personnes', component: PersonComponent },
    { path: 'personne/list', component: PersonComponent },
    { path: 'pesage/list', component: ListPesageComponent },
    { path: 'user', component: ListUserComponent },
    { path: 'consulter_pesage', component: PrixPesageComponent },
    { path: 'personne/add', component: AddPersonComponent },
    { path: 'user/add', component: AddUserComponent },
    { path: 'icons', component: IconsComponent }

];
