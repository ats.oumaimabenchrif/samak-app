import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NguiMapModule} from '@ngui/map';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { IconsComponent } from '../../icons/icons.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { MaterialModule } from 'app/Core/Material/material.module';
import { AddPersonComponent } from 'app/components/personne/add-person/add-person.component';
import { PersonComponent } from 'app/components/personne/person.component';
import { EditPersonComponent } from 'app/components/personne/edit-person/edit-person.component';
import { ListePesagePersooneComponent } from 'app/components/pesage/Liste-pesage-persoone/liste-pesage-persoone.component';
import { AddPesageComponent } from 'app/components/pesage/add-pesage/add-pesage.component';
import { ListPesageComponent } from 'app/components/pesage/liste-pesage/list-pesage.component';
import { EditPesageComponent } from 'app/components/pesage/edit-pesage/edit-pesage.component';
import { PrixPesageComponent } from 'app/components/pesage/prix-pesage/prix-pesage.component';
import { AddUserComponent } from 'app/components/Users/add-user/add-user.component';
import { EditUserComponent } from 'app/components/Users/edit-user/edit-user.component';
import { ListUserComponent } from 'app/components/Users/list-user/list-user.component';
import { ToastrService } from 'app/Core/Services/Toastr/toastr.service';
import { DashboardComponent } from 'app/components/dashboard/dashboard.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    NguiMapModule.forRoot({apiUrl: 'https://maps.google.com/maps/api/js?key=YOUR_KEY_HERE'})
  ],
  declarations: [
    AddPersonComponent,
    PersonComponent,
    IconsComponent,
    NotificationsComponent,
    EditPersonComponent,
    ListePesagePersooneComponent,
    AddPesageComponent,
    ListPesageComponent,
    EditPesageComponent,
    PrixPesageComponent,
    AddUserComponent,
    ListUserComponent,
    EditUserComponent, 
    DashboardComponent
  ],
  entryComponents:[
    PersonComponent,
    ListePesagePersooneComponent,
    EditPersonComponent,
    EditPesageComponent,
    AddPesageComponent,
    EditUserComponent,
    AddPersonComponent],
    providers:[ToastrService]

    
  
})

export class AdminLayoutModule {}
