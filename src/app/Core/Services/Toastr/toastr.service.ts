import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';
// declare var Toastr:any;
@Injectable({
  providedIn: 'root'
})
export class ToastrService {

  constructor(public snakbar:MatSnackBar) { }
  // Success(title:string,message?:string){
  //   Toastr.Success(title,message);
  // }
  // Warning(title:string,message?:string){
  //   Toastr.Warning(title,message);
  // }
  // Error(title:string,message?:string){
  //   Toastr.Error(title,message);
  // }
  // info(message?:string){
  //   Toastr.Info(message);
  // }
  
  config:MatSnackBarConfig={
    duration:3000,
    horizontalPosition:'right',
    verticalPosition:'top'
  }
  
  succes(message){
    this.config["panelClass"]=["notification","succes"];
    this.snakbar.open(message,'',this.config);
  }

  errorr(message){
    this.config["panelClass"]=["notification","errorr"];
    this.snakbar.open(message,'',this.config);
  }
}
