import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import{constant}from 'app/core/Services/constant'
import { Personne } from 'app/shared/Models/personne';
import { Badge } from 'app/shared/Models/badge';

const API = constant.url + 'Badges';

@Injectable({
  providedIn: 'root'
})
export class BadgeService {

  constructor(private http: HttpClient) {
  }

  getBadge() {
    return this.http.get(`${API}`);
  }
  getOnebedge(numeroS: string) {
    return this.http.get(`${API}` + '/numeroS/' + numeroS)
  }
  postBadge(numBadge:Badge) {
    return this.http.post(`${API}`, numBadge);
  }


  deletePersonne(id) {
    return this.http.delete(`${API}` + '/' + id);
  }
}
