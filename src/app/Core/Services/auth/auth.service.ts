import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { constant } from 'app/core/Services/constant'
import { from, Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
const API = constant.url + 'users';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // username:string=null;
  id: number;
  connect: boolean;

  constructor(private http: HttpClient) { }

  getUser(username: string, password: string) {
    return this.http.get(`${API}` + '/userexists/' + username + '/' + password);
  }

  getAllUser() {
    return this.http.get(`${API}`);
  }

  lastLogin() {
    return this.http.get('http://192.168.1.200:1040/api/logins/lastLogin');
  }

  login(data) {
    return this.http.post(`http://192.168.1.24:5000/api/auth/login`, data)
      .pipe(
        tap(_ => this.log('login')),
        catchError(this.handleError('login', []))
      );
  }

  logout(data) {
    return this.http.post(`http://192.168.1.24:5000/api/auth/logout`, data)
      .pipe(
        tap(_ => this.log('login')),
        catchError(this.handleError('login', []))
      );
  }

  getUsers() {
    return this.http.get('http://192.168.1.24:5000/api/users');
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(message);
  }
} 
