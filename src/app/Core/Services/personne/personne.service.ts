import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import{constant}from 'app/core/Services/constant'
import { Personne } from 'app/shared/Models/personne';

const API = constant.url + 'Personnes';

@Injectable({
  providedIn: 'root'
})
export class PersonneService {


  constructor(private http: HttpClient) {
  }


  getPersonne() {
    return this.http.get(`${API}`);
  }
  getOnePersonne(matricule: string) {
    return this.http.get(`${API}` + '/matricule/' + matricule)
  }
  postPersonne(data:Personne) {
    return this.http.post(`${API}`, data);
  }

  putPersonne(id, data:Personne) {
    return this.http.put(`${API}` + '/' + id, data);
  }

  deletePersonne(id) {
    return this.http.delete(`${API}` + '/' + id);
  }
}
