
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import{constant}from 'app/core/Services/constant'
import { Pesage } from 'app/shared/Models/pesage';

const API = constant.url + 'pesages';
@Injectable({
  providedIn: 'root'
})
export class PesageService {

  constructor(private http: HttpClient) { }

  //CRUD Pesage 
  getPesage() {
    return this.http.get(`${API}`);
  }

  getAllPesages() {
    return this.http.get<any>(`${API}`+'/all');
  }
  // 
  getPesageToday() {
    return this.http.get(`${API}`+'/today');
  }
  getAllPesagePersonne(matricule: string) {
    return this.http.get<any>(`${API}` + '/Pesagespersonne/' + matricule);
  }
  getPesageDate(date) {
    return this.http.get(`${API}` + "/le/" + date);
  }
  getPesagePeriod(db, df) {
    return this.http.get(`${API}` + "/du/" + db + "/au/" + df);
  }
  getPesagePeriodPersonne(mat, db, df) {
    return this.http.get(`${API}` + "/matricule/" + mat + "/du/" + db + "/au/" + df);
  }
  postPesage(data){
    return this.http.post(`${API}`, data);
  }
  exporterToExcel() {
    return this.http.get(`${API}` + "/export");
  }
  
  putPesage(id, data:Pesage) {
    return this.http.put(`${API}` + '/' + id, data)
  }

  deletePesage(id) {
    return this.http.delete(`${API}` + '/' + id);
  }

  getAllPesagesToday() {
    return this.http.get<any>(`${API}`+'/get');
  }
  getRechercherMatPesages(mat) {
    return this.http.get<any>(`${API}`+'/rechercher/'+mat);
  }
  getTotalPesagesPeriod(db,df) {
    // return this.http.get<any>(`${API}`+'/total/'+db+'/au/'+df);
    return this.http.get<any>(`${API}`+'/total/'+db+'/au/'+df+'/'+null);

  }

  getTotalPesagesPeriodPersonne(mat,db,df) {
    return this.http.get<any>(`${API}`+'/total/'+mat+'/du/'+db+'/au/'+df);
  }
  //exporter
  exportToExcel(date) {
    return this.http.get(`${API}` + '/' + date.startDate + '/' + date.endDate);
  }
  getPrixTotal(prix,du,au){
    return this.http.get(`${API}` + '/getPrixTotal/' + prix + '/' + du+'/'+au);
  }
}