import { Injectable } from '@angular/core';
import { constant } from 'app/core/Services/constant'
import { HttpClient } from '@angular/common/http';
const API = constant.url + 'users';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private http: HttpClient) {
  }

  getUser() {
    return this.http.get(`${API}`);
  }

  postUser(data) {
    return this.http.post(`${API}`, data);
  }

  putUser(id, data) {
    return this.http.put(`${API}` + '/' + id, data);
  }

  deleteUser(id) {
    return this.http.delete(`${API}` + '/' + id);
  }

  login(username: string, password: string) {
    return this.http.get(`${API}` + '/UserExists/' + username + '/' + password);
  }

}
