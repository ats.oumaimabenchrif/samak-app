import { NgModule } from '@angular/core';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatDividerModule } from '@angular/material/divider';
import { MatTableModule } from '@angular/material';
import { MatTabsModule } from '@angular/material/tabs';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatToolbarModule } from '@angular/material';
import {

  MatFormFieldModule,
  MatInputModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatSidenavModule,
  MatProgressSpinnerModule,
  MatCardModule,
  MatSelectModule,
  MatDialogModule,
  MatListModule,
  MatMenuModule,
  MatSortModule,
  MatAutocompleteModule,
  MatStepperModule
} from '@angular/material';

const material = [
  MatFormFieldModule,
  MatInputModule,
  MatGridListModule,
  MatPaginatorModule,
  MatCheckboxModule,
  MatDividerModule,
  MatButtonToggleModule,
  MatTableModule,
  MatMenuModule,
  MatSnackBarModule,
  MatSelectModule,
  MatCardModule,
  MatListModule,
  MatIconModule,
  MatSidenavModule,
  MatButtonModule,
  MatSortModule,
  MatProgressSpinnerModule,
  MatDialogModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatToolbarModule,
  MatAutocompleteModule,
  MatStepperModule,
  MatTabsModule,

];

@NgModule({

  imports: [material],
  exports: [material]
})
export class MaterialModule { }
