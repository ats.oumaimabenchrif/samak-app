import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { UserService } from 'app/core/services/user/user.service';
import { User } from 'app/shared/Models/user';
import { AddUserComponent } from '../add-user/add-user.component';
import { EditUserComponent } from '../edit-user/edit-user.component';
import { Router } from '@angular/router';
import { ToastrService } from 'app/Core/Services/Toastr/toastr.service';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.scss']
})
export class ListUserComponent implements OnInit {

  constructor(private dialog: MatDialog,
    private route: Router, 
    private toastr:ToastrService,
    private userService: UserService) { }
  user: User;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['nom', 'prenom', 'phone', 'username', 'password', 'role', 'Actions'];

  ngOnInit() {
    this.userService.getUser().subscribe(
      res => {
        this.dataSource = new MatTableDataSource(res as User[])
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }, err => { }
    )
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  UserAjouter() {
    this.route.navigate(['user/add']);
  }


  SupprimerUser(id) {
    if (confirm("Vous êtes sûr de supprimer?")) {
      this.userService.deleteUser(id).subscribe(res => {
        this.toastr.succes("La suppression avec succès")
        this.ngOnInit();
      }, err => {
        this.toastr.errorr("Error")
      })

    } else {
      this.toastr.succes("Merci!")
    }
  }
  UserModifier(element) {
    const dialogRef = this.dialog.open(EditUserComponent, {
      width: '70%',
      data: element
    });
  }
}