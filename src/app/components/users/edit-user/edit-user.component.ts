import { Component, OnInit, Inject } from '@angular/core';
import { ListUserComponent } from '../list-user/list-user.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { User } from 'app/shared/Models/user';
import { UserService } from 'app/core/services/user/user.service';
import { ToastrService } from 'app/Core/Services/Toastr/toastr.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {

  constructor(private builder: FormBuilder,
    private userServer:UserService,
    private toastr:ToastrService,
    private dialogRef: MatDialogRef<ListUserComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any) { }
  formGroupeUser: FormGroup;
  user: User;
  ngOnInit() {
    this.formGroupeUser = this.builder.group({
      id: new FormControl(this.data.id),
      nom: new FormControl(this.data.nom),
      prenom: new FormControl(this.data.prenom),
      username: new FormControl(this.data.username),
      password: new FormControl(this.data.password),
      phone: new FormControl(this.data.phone),
      role: new FormControl(this.data.role),
    })
  }

  ModifierUser() {
    this.user = this.formGroupeUser.value;
    if (confirm("Vous êtes sûr de modifier cette personne?")) {
      this.userServer.putUser(this.data.id, this.user).subscribe(
        o => {
        this.toastr.succes("La modification fait avec succès");
        }, err => {
          this.toastr.errorr("Erreur")
        }
      )
    } else { this.toastr.succes("Merci") }
  }

}
