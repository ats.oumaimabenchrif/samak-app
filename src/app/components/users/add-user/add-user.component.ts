import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { User } from 'app/shared/Models/user';
import { UserService } from 'app/core/services/user/user.service';
import { Router } from '@angular/router';
import { ToastrService } from 'app/Core/Services/Toastr/toastr.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {

  constructor(private builder: FormBuilder,
    private route: Router, 
    private toastr:ToastrService,
    private useService: UserService) { }
  formGroupeUser: FormGroup;
  user: User;

  ngOnInit() {
    this.formGroupeUser = this.builder.group({
      id: new FormControl(0),
      nom: new FormControl(),
      prenom: new FormControl(),
      userName: new FormControl(),
      password: new FormControl(),
      phone: new FormControl(),
      role: new FormControl(),
    })
  }

  ajouteUser() {
    this.user = this.formGroupeUser.value;
    if (confirm("Vous êtes sûr d'Ajouter cette User??")) {

      this.useService.postUser(this.user).subscribe(
        res => {
          this.toastr.succes("bien ajouter")
          this.route.navigate(['User']);
        }, erro => {  this.toastr.errorr("error"); });
    } else { this.toastr.succes("Merci"); }
  }

  Annuler() {
    this.route.navigate(['User']);
  }


}
