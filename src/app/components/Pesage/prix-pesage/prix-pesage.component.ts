import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { PesageService } from 'app/core/services/pesage/pesage.service';
import { Pesage } from 'app/shared/Models/pesage';
import { PersonneService } from 'app/core/services/personne/personne.service';
import { EditPesageComponent } from '../edit-pesage/edit-pesage.component';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import * as moment from 'moment';
import { ToastrService } from 'app/Core/Services/Toastr/toastr.service';
@Component({
  selector: 'app-prix-pesage',
  templateUrl: './prix-pesage.component.html',
  styleUrls: ['./prix-pesage.component.scss']
})
export class PrixPesageComponent implements OnInit {

  constructor(private dialog: MatDialog,
    private builder: FormBuilder,
    private toastr:ToastrService,
    private pesageService: PesageService,
    private personneService: PersonneService) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['matricule', 'Personne','poid', 'datePesage', 'Heure', 'Actions'];

  matricule;
  dateDebut;
  dateFin;
  ngOnInit() {


    this.pesageService.getPesageToday().subscribe(
      res => {

        this.dataSource = new MatTableDataSource(res as Pesage[]);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

      }, err => {
        console.log("erreur");
      }
    );
    this.dateDebut = moment(new Date()).format('YYYY-MM-DD');
    this.dateFin = moment(new Date()).format('YYYY-MM-DD');
  }

  Supprimerpesage(id) {
    if (confirm("Vous êtes sur de faire cette suppression?")) {
      this.pesageService.deletePesage(id).subscribe(
        res => {
         this.toastr.succes("La Suppression fait avec succès") ;
          this.ngOnInit();
        }, err => {
          this.toastr.succes("Erreur de suppression");
        });
    } else {  this.toastr.succes("Merci"); }
  }

  opendialogModifier(element) {
    const dialogRef = this.dialog.open(EditPesageComponent, {
      width: '70%',
      data: element
    });
  }
  search() {
    var db: string = moment(this.dateDebut).format('YYYY-MM-DD');
    var df: string = moment(this.dateFin).format('YYYY-MM-DD');
    var matricule: string = this.matricule as string;
    if (matricule == null && db>=df &&db!=null && df!=null) {
      alert('La date début est supérieur à date fin')
      this.pesageService.getPesagePeriod(db, df).subscribe(
        res => {
          this.dataSource = new MatTableDataSource(res as Pesage[]);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        },
        err => {
          
        }
      );
    } else {
     
      if (matricule != null) {
        this.pesageService.getPesagePeriodPersonne(matricule, db, df).subscribe(
          res => {
            this.dataSource = new MatTableDataSource(res as Pesage[]);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
          },
          err => {
           
          }
        );
      }
    }

  }
}
