import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Pesage } from 'app/shared/Models/pesage';
import * as moment from 'moment';
import { EditPesageComponent } from '../edit-pesage/edit-pesage.component';
import { FormBuilder } from '@angular/forms';
import { PesageService } from 'app/core/services/pesage/pesage.service';
import { PersonneService } from 'app/core/services/personne/personne.service';

import { ToastrService } from 'app/Core/Services/Toastr/toastr.service';

import { PersonComponent } from 'app/components/personne/person.component';

@Component({
  selector: 'app-liste-pesage-persoone',
  templateUrl: './liste-pesage-persoone.component.html',
  styleUrls: ['./liste-pesage-persoone.component.scss']
})
export class ListePesagePersooneComponent implements OnInit {


  constructor(private dialog: MatDialog,
    private builder: FormBuilder,
    private pesageService: PesageService,
    private toastr:ToastrService,
    private personneService: PersonneService,
    private dialogRef: MatDialogRef<PersonComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['matricule', 'Personne', 'poid', 'datePesage', 'Heure', 'Actions'];

  matricule;
  dateDebut;
  dateFin;

  ngOnInit() {
    this.pesageService.getAllPesagePersonne(this.data).subscribe(
      res => {

        this.dataSource = new MatTableDataSource(res as Pesage[]);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

      }, err => {
        console.log("erreur");
      }
    );
    this.dateDebut = moment(new Date()).format('YYYY-MM-DD');
    this.dateFin = moment(new Date()).format('YYYY-MM-DD');
  }

  Supprimerpesage(id) {
    if (confirm("Vous êtes sur de faire cette suppression?")) {
      this.pesageService.deletePesage(id).subscribe(
        res => {
          this.toastr.succes("La Suppression fait avec succès");
          this.ngOnInit();
        }, err => {
          this.toastr.errorr("Erreur de suppression")
        });
    } else {  this.toastr.succes("Merci"); }
  }

  opendialogModifier(element) {
    const dialogRef = this.dialog.open(EditPesageComponent, {
      width: '70%',
      data: element
    });
  }

  search(){
    var idPersonne=parseInt(this.data) ; 
    var db: string = moment(this.dateDebut).format('YYYY-MM-DD');
    var df: string = moment(this.dateFin).format('YYYY-MM-DD');
    console.log(idPersonne,db,df);
     
      this.pesageService.getPesagePeriodPersonne(idPersonne,db, df).subscribe(
        res => {
          this.dataSource = new MatTableDataSource(res as Pesage[]);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        },
        err => {});} 
  
}
