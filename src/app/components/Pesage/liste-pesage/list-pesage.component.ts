import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { PesageService } from 'app/core/services/pesage/pesage.service';
import { Pesage } from 'app/shared/Models/pesage';
import { PersonneService } from 'app/core/services/personne/personne.service';
import { EditPesageComponent } from '../edit-pesage/edit-pesage.component';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import * as moment from 'moment';
import { ToastrService } from 'app/Core/Services/Toastr/toastr.service';

@Component({
  selector: 'app-list-pesage',
  templateUrl: './list-pesage.component.html',
  styleUrls: ['./list-pesage.component.scss']
})

export class ListPesageComponent implements OnInit {

  constructor(private dialog: MatDialog,
    private builder: FormBuilder,
    private toastr:ToastrService,
    private pesageService: PesageService,
    private personneService: PersonneService) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['matricule', 'Personne', 'totalpesages', 'PrixTotale'];

  allData;
  matricule;
  dateDebut;
  dateFin;
  Prix;
  totalpesages
  ngOnInit() {

    this.pesageService.getAllPesagesToday().subscribe(
      res => {
        this.dataSource = new MatTableDataSource(res as Pesage[]);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }, err => {
        console.log("erreur");
      }
    );
    this.dateDebut = moment(new Date()).format('YYYY-MM-DD');
    this.dateFin = moment(new Date()).format('YYYY-MM-DD');

  }
  Mffonction() {
    this.pesageService.getPesage().subscribe((res: any) => {
      console.log('Pesages: ', res);
      this.allData = res;
      const today = res.filter(x => x.datePesage === (moment().format('YYYY-MM-DD') + 'T00:00:00'));
      console.log('Today: ', today);
      const pesage = today.reduce((m, o) => {
        console.log('M: ', m);
        console.log('O: ', o);
        // const elt = m.find(x => x.id == o.id);
        // console.log('Elt: ', elt);
      })
      this.dataSource = new MatTableDataSource(today);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

    });
  }
  Supprimerpesage(id) {
    if (confirm("Vous êtes sur de faire cette suppression?")) {
      this.pesageService.deletePesage(id).subscribe(
        res => {
          this.toastr.succes("La Suppression fait avec succès");
          this.ngOnInit();
        }, err => {
          this.toastr.errorr("Erreur de suppression")
        });
    } else { this.toastr.succes("Merci"); }
  }

  opendialogModifier(element) {
    const dialogRef = this.dialog.open(EditPesageComponent, {
      width: '70%',
      data: element
    });
  }
  search() {
    var prix: number = this.Prix as number;
    var db: string = moment(this.dateDebut).format('YYYY-MM-DD');
    var df: string = moment(this.dateFin).format('YYYY-MM-DD');
    var matricule: string = this.matricule as string;
    if (this.dateDebut == null)
      db = moment(new Date()).format('YYYY-MM-DD');
    if (this.dateFin == null)
      df = moment(new Date()).format('YYYY-MM-DD');
    if (this.Prix == null)
      prix = 0;
    if (matricule != null) {
      this.pesageService.getTotalPesagesPeriodPersonne(matricule, db, df).subscribe(
        res => {
          this.dataSource = new MatTableDataSource(res as Pesage[]);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }, err => { console.log(err); });
    }
    else if (matricule == null) {

      this.pesageService.getTotalPesagesPeriod(db, df).subscribe(
        res => {
          this.dataSource = new MatTableDataSource(res as Pesage[]);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }, err => { console.log(err) });
    }
  }

  CalculePrixTotal() {

    var prix: number = this.Prix as number;
    var db = moment(this.dateDebut).format('YYYY-MM-DD');
    var df = moment(this.dateFin).format('YYYY-MM-DD');
    if (this.dateDebut == null)
      db = moment(new Date()).format('YYYY-MM-DD');
    if (this.dateFin == null)
      df = moment(new Date()).format('YYYY-MM-DD');
    if (this.Prix == null)
      prix = 0;
    this.pesageService.getPrixTotal(prix, db, df).subscribe(
      res => {
        this.dataSource = new MatTableDataSource(res as any[]);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      err => {
        console.log(err);
      }
    )
  }

}


