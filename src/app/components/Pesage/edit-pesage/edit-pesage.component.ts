import { Component, OnInit, Inject } from '@angular/core';
import { ListPesageComponent } from '../liste-pesage/list-pesage.component';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { PesageService } from 'app/core/services/pesage/pesage.service';
import { Pesage } from 'app/shared/Models/pesage';
import { ToastrService } from 'app/Core/Services/Toastr/toastr.service';

@Component({
  selector: 'app-edit-pesage',
  templateUrl: './edit-pesage.component.html',
  styleUrls: ['./edit-pesage.component.scss']
})
export class EditPesageComponent implements OnInit {

  constructor(private builder: FormBuilder,
    private toastr: ToastrService,
    private dialogRef: MatDialogRef<ListPesageComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private pesageService: PesageService) { }
  
  formGroupPasage: FormGroup;
  pesage: Pesage;
  pesageall: Pesage[];
  ngOnInit() {
    console.log(this.data.idPersonneNavigation.id, this.data.idUser)
    this.formGroupPasage = this.builder.group({
      id: new FormControl(this.data.id),
      poid: new FormControl(this.data.poid),
      datePesage: new FormControl(this.data.datePesage),
      idPersonne: new FormControl(this.data.idPersonneNavigation.id),
      matricule: new FormControl(this.data.idPersonneNavigation.matricule),
      idUser: new FormControl(this.data.idUser)
    });
  }

  ModifierPesage() {
    this.pesage = this.formGroupPasage.value;
    console.log(this.pesage);
    if (confirm("Vous étes sur de faire cette modifier?")) {
      this.pesageService.putPesage(this.data.id, this.pesage).subscribe(
        res => {
          this.toastr.succes("La Modification fait avec succès");
        }, err => {
          this.toastr.succes("Erreur de  Modifier")
        });

    } else { this.toastr.succes("Merci!!"); }
  }


}
