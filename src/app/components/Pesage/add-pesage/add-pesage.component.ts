import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ListPesageComponent } from '../liste-pesage/list-pesage.component';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { User } from 'app/shared/Models/user';
import { UserService } from 'app/core/services/user/user.service';
import { Personne } from 'app/shared/Models/personne';
import { PersonneService } from 'app/core/services/personne/personne.service';
import { PesageService } from 'app/core/services/pesage/pesage.service';
import { Pesage } from 'app/shared/Models/pesage';
import { BadgeService } from '../../../core/services/badge/badge.service';
import { Badge } from 'app/shared/Models/badge';
import { ToastrService } from 'app/Core/Services/Toastr/toastr.service';


@Component({
  selector: 'app-add-pesage',
  templateUrl: './add-pesage.component.html',
  styleUrls: ['./add-pesage.component.scss']
})
export class AddPesageComponent implements OnInit {

  constructor(private builder: FormBuilder,
    private userService: UserService,
    private toastr:ToastrService,
    private userpersonne: PersonneService,
    private userpesage: PesageService,
    private dialogRef: MatDialogRef<ListPesageComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any) { }
   
  formGroupPasage: FormGroup;
  alluser: User[];
  allallPersonne: Personne[];
  pesage: Pesage;
  pesageliste: Pesage[];
  ngOnInit() {
    console.log(this.data.idbadge);
    this.formGroupPasage = this.builder.group({
      id: new FormControl(0),
      poid: new FormControl(),
      datePesage: new FormControl(),
      matricule: new FormControl(this.data.matricule),
      idPersonne: new FormControl(this.data.id),
      idUser: new FormControl(this.data.idUser),
      // idbadge: new FormControl(this.data.idbadge),
    });
    this.userService.getUser().subscribe(
      res => { this.alluser = res as User[] }, err => { }
    )
    this.userpersonne.getPersonne().subscribe(
      res => { this.allallPersonne = res as Personne[] }, err => { }
    )

    
  }

  ajoutePesage() {
    var pesage: Pesage;
    pesage = this.formGroupPasage.value;
    console.log(pesage);
    if (confirm("Vous étes sur de faire cette d'ajouter?")) {
      this.userpesage.postPesage(pesage).subscribe(
        res => {
          this.toastr.succes("pesage bien ajouter");
        }, err => {
          console.log("error"); this.toastr.errorr("Error");
        }
      )
    } else {  this.toastr.succes("Merci!"); } 
  }

  updateuser(idpersonne) {
    var personnee = this.allallPersonne.find(l => l.id == idpersonne);
    this.formGroupPasage.controls['matricule'].patchValue(personnee.matricule);
  }

}
