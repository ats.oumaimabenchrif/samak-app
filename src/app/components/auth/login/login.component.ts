import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { UserService } from 'app/core/services/user/user.service';
import { Router } from '@angular/router';
import { ToastrService } from 'app/Core/Services/Toastr/toastr.service';
import { AuthService } from 'app/core/services/auth/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {


  constructor(private router: Router,
    private builder: FormBuilder,
    private UserService: UserService,
    private authService: AuthService, private toastr: ToastrService) { }

  UserformGroup: FormGroup;

  ngOnInit() {
    this.UserformGroup = this.builder.group({
      // id: new FormControl(0),
      username: new FormControl(),
      password: new FormControl(),
    });
    this.authService.getUsers().subscribe(res => console.log('Users: ', res));
  }

  login() {
    
    if (this.UserformGroup.invalid) {
      return;
    }

    const data = {
      username: this.UserformGroup.controls.username.value,
      password: this.UserformGroup.controls.password.value
    }
    console.log('Login', this.UserformGroup.value);
    this.authService.login(data).subscribe((res: any) => {
      console.log('Res: ', res);
      if (res.token) {
        localStorage.setItem('token', res.token);
        localStorage.setItem('username', res.username);
        if (res.role) {
          localStorage.setItem('role', res.role);
          if (res.role === 'admin') {
            this.router.navigate(['personnes']);
          } else {
            this.router.navigate(['login']);
          }
        }

      }

    }, err => console.log('Error: ', err));

  }

}
