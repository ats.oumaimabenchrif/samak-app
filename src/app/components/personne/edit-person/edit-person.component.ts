import { Component, OnInit, Inject } from '@angular/core';

import { FormGroup, FormBuilder, FormControl, NgForm } from '@angular/forms';
import { PersonneService } from 'app/core/services/personne/personne.service';
import { Pesage } from 'app/shared/Models/pesage';
import { Personne } from 'app/shared/Models/personne';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Badge } from 'app/shared/Models/badge';
import { BadgeService } from 'app/core/services/badge/badge.service';
import { ToastrService } from 'app/Core/Services/Toastr/toastr.service';
import { PersonComponent } from '../person.component';

@Component({
  selector: 'app-edit-person',
  templateUrl: './edit-person.component.html',
  styleUrls: ['./edit-person.component.scss']
})
export class EditPersonComponent implements OnInit {

  constructor(private builder: FormBuilder,
    private personneservice: PersonneService,
    private badgeService: BadgeService,
    private toastr:ToastrService,
    private dialogRef: MatDialogRef<PersonComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any) { }

  badges: Badge[];
  formGroupePersonne: FormGroup;
  personne: Personne;
  ngOnInit() {


    this.formGroupePersonne = this.builder.group({
      id: new FormControl(this.data.id),
      nom: new FormControl(this.data.nom),
      prenom: new FormControl(this.data.prenom),
      phone: new FormControl(this.data.phone),
      cin: new FormControl(this.data.phone),
      cnss: new FormControl(this.data.cnss),
      matricule: new FormControl(this.data.matricule),
      adresse: new FormControl(this.data.adresse),
      ville: new FormControl(this.data.ville),
      situation: new FormControl(this.data.situation),
      nombreEnfant: new FormControl(this.data.nombreEnfant),
      dateNaissance: new FormControl(new Date(this.data.dateNaissance)),
      dateEmbauche: new FormControl(this.data.dateEmbauche),
      dateArret: new FormControl(this.data.dateArret),
      idbadge: new FormControl(this.data.idbadge),
    });
    this.badgeService.getBadge().subscribe(res =>
      this.badges = res as Badge[]);
  }

  ModifierPersonne() {
    this.personne = this.formGroupePersonne.value;
    if (confirm("Vous êtes sûr de modifier cette personne?")) {
      this.personneservice.putPersonne(this.data.id, this.personne).subscribe(
        o => {
          console.log('Bien Modifier');
          this.toastr.succes("La modification fait avec succès")
        }, err => {
          console.log('Erreur');
          this.toastr.errorr('Erreur');
        }
      )
    } else {  this.toastr.succes("Merci") }

  }
}
