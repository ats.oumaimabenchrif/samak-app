import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatDialog, MatPaginator } from '@angular/material';
import { PersonneService } from 'app/core/services/personne/personne.service';
import { Personne } from 'app/shared/Models/personne';
import { MatSort } from '@angular/material/sort';
import { AddPesageComponent } from '../pesage/add-pesage/add-pesage.component';
import { Router } from '@angular/router';
import { ListePesagePersooneComponent } from '../pesage/Liste-pesage-persoone/liste-pesage-persoone.component';
import { ToastrService } from 'app/Core/Services/Toastr/toastr.service';

import { EditPersonComponent } from './edit-person/edit-person.component';


@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.scss']
})
export class PersonComponent implements OnInit {

  constructor(private personneService: PersonneService,
    private route:Router, 
    private toastr:ToastrService,
    private dialog: MatDialog
  ) { }


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['nom', 'prenom', 'phone', 'cin', 'cnss', 'situation', 'ville', 'dateEmbauche', 'matricule','Badge', 'Actions'];


  ngOnInit() {
    this.personneService.getPersonne().subscribe(
      res => {

        this.dataSource = new MatTableDataSource(res as Personne[]);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    );
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  opendialogModifier(element) {
    const dialogRef = this.dialog.open(EditPersonComponent, {
      width: '70%',
      data: element
    });
  }
  Supprimerpersonne(id) {
   
    if (confirm("vous êtes sur de supprimer cette personne?")) {
      this.personneService.deletePersonne(id).subscribe(
        res => {
          this.toastr.succes('Bien Supprimer');
        
          this.ngOnInit();
        }, err => {
          this.toastr.errorr('erreur1');
        })

    }else{  this.toastr.succes("Merci");} }
     
    
    Ajouterpesage(element) {
      const dialogRef = this.dialog.open(AddPesageComponent, {
        width: '70%',
        data: element
      });
    }

    AjouterPersonne(){
      this.route.navigate(['personne/add']); 
    }
    consulterpesage(id){
      const dialogRef = this.dialog.open(ListePesagePersooneComponent, {
        width: '70%',
        data: id
      });
    }
}
