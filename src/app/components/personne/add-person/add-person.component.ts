import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { PersonneService } from 'app/core/services/personne/personne.service';
import { Personne } from 'app/shared/Models/personne';
import { BadgeService } from 'app/core/services/badge/badge.service';
import { Badge } from 'app/shared/Models/badge';
import { Router } from '@angular/router';
import { ToastrService } from 'app/Core/Services/Toastr/toastr.service';

@Component({
  selector: 'app-add-person',
  templateUrl: './add-person.component.html',
  styleUrls: ['./add-person.component.scss']
})
export class AddPersonComponent implements OnInit {
  
  constructor(private builder: FormBuilder,
    private route: Router,
    private toastr:ToastrService,
    private badgeService: BadgeService,
    private service: PersonneService) { }
  badges: Badge[];
  formGroupePersonne: FormGroup;
  ngOnInit() {
    this.formGroupePersonne = this.builder.group({
      id: new FormControl(0),
      nom: new FormControl(),
      prenom: new FormControl(),
      phone: new FormControl(),
      cin: new FormControl(),
      cnss: new FormControl(),
      matricule: new FormControl(),
      adresse: new FormControl(),
      ville: new FormControl(),
      situation: new FormControl(),
      nombreEnfant: new FormControl(),
      dateNaissance: new FormControl(),
      dateEmbauche: new FormControl(),
      // dateArret: new FormControl(null),
      idbadge: new FormControl(),
    });
    this.badgeService.getBadge().subscribe(res =>
      this.badges = res as Badge[]);
  }

  ajoutePersonne() {

    var personne: Personne;
    personne = this.formGroupePersonne.value;
    var nombreEnfant: number = this.formGroupePersonne.controls.nombreEnfant.value;
    this.formGroupePersonne.controls.nombreEnfant.patchValue(nombreEnfant);
    if (confirm("Voues êtes sûr d'ajouter cette Personne?")) {
      this.service.postPersonne(personne).subscribe(
        res => {
          console.log('ajoute avec succes');
          this.toastr.succes("Ajouter avec succes");
          this.route.navigate(['personne/list']);
        },
        err => { console.log('n\'ajoute pas');
        this.toastr.errorr("Erreur d'ajout"); })
    } else {  this.toastr.succes("Merci!") }
  }
  Annuler(){
    this.route.navigate(['personne/list']);
  }

}
