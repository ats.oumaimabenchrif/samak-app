import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  loading: boolean = true;

  constructor(public location: Location, private router: Router) {
    /*this.router.events.subscribe((routerEvent: any) => {
      this.checkRouterEvent(routerEvent);
    });*/
  }

  ngOnInit() {
  }

  isMap(path) {
    var titlee = this.location.prepareExternalUrl(this.location.path());
    titlee = titlee.slice(1);
    if (path == titlee) {
      return false;
    }
    else {
      return true;
    }
  }

  checkRouterEvent(routerEvent: Event): void {
    console.log('Router Events -----> ', routerEvent);
    if (routerEvent instanceof NavigationStart) {
        this.loading = true;
    }

    if (routerEvent instanceof NavigationEnd ||
        routerEvent instanceof NavigationCancel ||
        routerEvent instanceof NavigationError) {
        this.loading = false;
    }
}
}
