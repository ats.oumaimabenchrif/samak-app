import { Component, OnInit } from '@angular/core';
import { AuthService } from 'app/core/services/auth/auth.service';

declare const $: any;
declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}
export const ROUTES: RouteInfo[] = [
  { path: '/dashboard', title: 'Dashboard', icon: 'pe-7s-graph', class: '' },
  { path: '/personne/list', title: 'Employés', icon: 'pe-7s-id', class: '' },
  { path: '/pesage/list', title: 'Pesages', icon: 'pe-7s-note2', class: '' },
  { path: '/user', title: 'Utilisateurs', icon: 'pe-7s-users', class: '' },
  { path: '/consulter_pesage', title: 'Consulter les pesages', icon: 'pe-7s-albums', class: '' },
  // { path: '/icon', title: 'icon',  icon:'pe-7s-note2', class: '' },
  { path: '/login', title: 'Logout', icon: 'pe-7s-back' /*icon:'pe-7s-rocket'*/, class: 'active-pro' },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  };

  logout() {
    const user = {
      username: localStorage.getItem('username') ? localStorage.getItem('username') : 'Anonymous'
    };
    console.log('Username: ', user);
    this.authService.logout(user).subscribe(res => {
      console.log('res: ', res);
    });
  }
}
